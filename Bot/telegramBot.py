from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, CallbackContext
from Material.basic import getConfig, getResume, getResumeSection
import re

# 定義cmd選項
def main():
    # 讀取env設定檔
    botKey = getConfig('telegram', 'bot_key')

    # 設定tg
    update = Updater(token=botKey)
    dispatcher = update.dispatcher

    dispatcher.add_handler(CommandHandler('resume', resumeCmd))
    dispatcher.add_handler(CallbackQueryHandler(sysResponse))
    dispatcher.add_handler(CommandHandler('help', helpCmd))

    update.start_polling()
    update.idle()


# 履歷
def resumeCmd(update: Update, context: CallbackContext):
    keyboards = []
    keyboardTemp = []

    sections = getResumeSection()
    for section in sections:
        keyboardTemp.extend([
            InlineKeyboardButton(section, callback_data=f'resume_{section}')
        ])

        if len(keyboardTemp) == 3:
            keyboards.append(keyboardTemp)
            keyboardTemp = []

    if keyboardTemp:
        keyboards.append(keyboardTemp)

    update.message.reply_text(
        '📒Resume Info📒', reply_markup=InlineKeyboardMarkup(keyboards)
    )


# Help
def helpCmd(update: Update, context: CallbackContext):
    mId = update.message.message_id
    update.message.reply_text('help!', reply_to_message_id=mId)


# 系統回覆
def sysResponse(update: Update, context: CallbackContext):
    query = update.callback_query
    data = query.data

    if 'resume_' in data:
        section = data.replace('resume_', '')
        resume = getResume(section)
        query.message.reply_text(f'📒Resume Info: {section}📒\n\n{resume}')
