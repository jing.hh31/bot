import pandas
import pygsheets
import requests
import re
import time
from bs4 import BeautifulSoup
from Material.basic import getConfig, getGoogle


def setSheet(title, data):
    print('...Google Set Sheet Start...')
    client = __getClient()

    # 依表單名稱取得 worksheet 物件
    worksheet = client.worksheet_by_title(title)

    # 設定dataframe表單，從A1開始
    if isinstance(data, dict):
        df = pandas.DataFrame(data)
        worksheet.set_dataframe(df, 'A1', copy_index=True, nan='')
        print('done')

    print('...Google Set Sheet End...')


def checkSheet(title, data):


def __getClient():
    filename = f'{getGoogle()}/googleKey.json'
    sheetKey = getConfig('googleKey', 'sheet_key')

    # Google表單連線
    authorize = pygsheets.authorize(service_account_file=filename)

    # 取得 spreadSheet 物件
    client = authorize.open_by_key(sheetKey)

    return client
