from Material import loggerManager as logger, constant as const
import configparser
import os
import pandas
import re
import sys

PWD = os.getcwd()
RESUME_PWD = f'{PWD}/resume.txt'


def getConfig(section, param):
    config = configparser.ConfigParser()
    config.read(f'{PWD}/.env.ini')

    return config.get(section, param)


def error(msg):
    print()
    print(f'Errorヽ(`Д´)ノ: {msg}')

    log = logger.setLogger(logger.ERROR_LOG)
    log.error(msg)

    sys.exit(2)


# 取得指定Google資料夾路徑
def getGoogle():
    return f'{PWD}/Google'


def getResume(section):
    resumeSection = getResumeSection()
    temp = list(resumeSection)
    endLine = 0
    output = ''

    try:
        startLine = resumeSection[section]

        if temp.index(section) != len(temp) - 1:
            nextIndex = temp.index(section) + 1
            endLine = resumeSection[temp[nextIndex]] - 1

        file = open(RESUME_PWD)
        allLines = file.readlines()

        file.close()

        if endLine:
            lines = allLines[startLine:endLine]
        else:
            lines = allLines[startLine]

        for line in lines:
            output += line

    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        error(
            f'FILE: {fname}, AT: Line.{exc_tb.tb_lineno}, ERROR_TYPE: {exc_type}, MESSAGE: {sys.exc_info()}')

        if file:
            file.close()

    return output


def getResumeSection():
    regex = re.compile(rf'## .* ##')
    section = {}

    try:
        file = open(RESUME_PWD)
        lines = file.readlines()
        getLines = 0

        for line in lines:
            getLines += 1

            if re.match(regex, line):
                key = line[3:len(line) - 4]
                section[key] = getLines

        file.close()

    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        error(
            f'FILE: {fname}, AT: Line.{exc_tb.tb_lineno}, ERROR_TYPE: {exc_type}, MESSAGE: {sys.exc_info()}')

        if file:
            file.close()

    return section


# 取得指定網址
def getUrl(name):
    urls = pandas.Series(data=[
        'https://isin.twse.com.tw/isin/single_main.jsp?',
        'https://www.twse.com.tw/exchangeReport/STOCK_DAY?response=json&',
        'https://www.twse.com.tw/fund/T86?response=html&',
    ], index=[
        const.company,
        const.stock,
        const.dailyInstitutionInvestor,
    ])

    return urls[name]
