from logging import handlers
import logging
import os
import sys

ERROR_LOG = 'error.log'
DIR_PATH = '../logs'


def setLogger(name):
    absPath = os.path.abspath(DIR_PATH)
    fileName = ("%s/%s" % (absPath, name))

    # 檢查檔案夾是否存在
    if not os.path.exists(absPath):
        os.makedirs(absPath)

    try:
        logger = logging.getLogger(fileName)
        handler = handlers.RotatingFileHandler(
            fileName, mode='a+', encoding='utf-8'
        )

        # LogRecord attributes: https://docs.python.org/3/library/logging.html
        loggingFormat = logging.Formatter(
            '%(asctime)s - %(levelname)s - %(filename)s - %(lineno)s - %(message)s'
        )

        handler.setFormatter(loggingFormat)

        if name == ERROR_LOG:
            handler.setLevel(logging.ERROR)
        else:
            handler.setLevel(logging.DEBUG)

        logger.addHandler(handler)
    except:
        print(f'Logger Error: {sys.exc_info()}')

    return logger
