from urllib.parse import urlencode
from bs4 import BeautifulSoup
import pandas
import re
import requests
import time
import sys
# 導入當前文件的上層目錄到搜尋路經中
sys.path.append('..')
from Material import basic, constant as const
from Material.googleManager import setSheet


def setCompany(*args):
    owncode = []
    stockname = []
    for arg in args:
        isMatchNumber = re.fullmatch(r'\d+', arg)

        if isMatchNumber:
            owncode.append(arg)
        else:
            stockname.append(arg)

    params = {'owncode': ','.join(owncode), 'stockname': ','.join(stockname)}

    results = requests.get(
        basic.getUrl(const.company),
        params=params,
        timeout=10
    )

    if results.status_code != 200:
        errorMsg = ("%s(%s)" % (results.text, results.status_code))

        return errorMsg

    # 檢查是否有資料
    soup = BeautifulSoup(results.text, 'html.parser')
    if soup.table is None:
        return 'Company Not Found'

    '''
    pandas.read_html(results.text)[0] 為 Dataframe
    [1:]表示 資料從row 1開始，取到最後一筆
    Columns: 0 ~ 9, Rows: 0 ~ 1
    有價證券代號 column 2
    有價證券名稱 column 3
    '''
    df = pandas.read_html(results.text)[0][1:]
    code = df[2].tolist()
    name = df[3].tolist()

    # 限制一次寫入前100筆就結束
    if len(code) > 100:
        code = [:100]
        name = [:100]

    # 記進表單
    setSheet(const.company, {'code': code, 'name': name})
